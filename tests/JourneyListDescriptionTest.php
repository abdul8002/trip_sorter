<?php

namespace Tests;

use App\JourneyList;

class JourneyListDescriptionTest extends \PHPUnit_Framework_TestCase
{
	
	/* @test */
	public function testAJourneyDescriptionIsCorrectGivenAnOrderedListOfBoardingPasses()
	{
		$boardingPasses = [
			['from' => 'Madrid', 'to' => 'Barcelona', 'transport_type' => 'train', 'transport_number' => '78A', 'seat_number' => '45B'],
			['from' => 'Barcelona', 'to' => 'Geneva Airport', 'transport_type' => 'airport bus', 'transport_number' => null],
		    ['from' => 'Geneval Airport', 'to' => 'Stockholm', 'transport_type' => 'flight', 'transport_number' => 'SK455', 'seat_number' => '3A', 'gate' => '45B', 'more_info' => 'Baggage drop at ticket counter 344.'],
		    ['from' => 'Stockholm', 'to' => 'New York JFK', 'transport_type' => 'flight', 'transport_number' => 'SK22', 'seat_number' => '7B', 'gate' => '22', 'more_info' => 'Baggage will we automatically transferred from your last leg.']
		];

		$expectedResult = [
			'1. Take train 78A from Madrid to Barcelona. Sit in 45B.',
			'2. Take the airport bus from Barcelona to Geneva Airport. No seat assignment.',
			'3. From Geneval Airport, take flight SK455 to Stockholm. Gate 45B, Seat 3A. Baggage drop at ticket counter 344.',
			'4. From Stockholm, take flight SK22 to New York JFK. Gate 22, Seat 7B. Baggage will we automatically transferred from your last leg.',
			'5. You have arrived at your destination.'
		];

		$result = (new JourneyList($boardingPasses))->describe();

		$this->assertEquals($expectedResult, $result);
	}
}