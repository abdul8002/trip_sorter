<?php

namespace Tests;

use App\LinearPathSort;

class SortTest extends \PHPUnit_Framework_TestCase
{
	
	/* @test */
	public function testAListOfUnsortedBoardingPassesAreSortedCorrectly()
	{
		$unsortedBoardingPasses = [
			['from' => 'a', 'to' => 'b'],
			['from' => 'c', 'to' => 'd'],
			['from' => 'b', 'to' => 'c'],
			['from' => 'e', 'to' => 'f'],
			['from' => 'd', 'to' => 'e']
		];

		$expectedResult = [
			['from' => 'a', 'to' => 'b'],
			['from' => 'b', 'to' => 'c'],
			['from' => 'c', 'to' => 'd'],
			['from' => 'd', 'to' => 'e'],
			['from' => 'e', 'to' => 'f'],
		];

		$result = (new LinearPathSort($unsortedBoardingPasses))->sort();

		$this->assertEquals($expectedResult, $result);
	}
}