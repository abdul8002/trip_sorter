<?php

namespace App;

use App\Contracts\JourneyDescriptionInterface;

class JourneyList implements JourneyDescriptionInterface
{
	/**
	 * The array of boarding passes.
	 * 
	 * @var array
	 */
	protected $passes;
	
	/**
	 * A sorted list of boarding passes to be described.
	 * 
	 * @param array $sortedBoardingPasses 
	 */
	public function __construct($sortedBoardingPasses)
	{
		$this->passes = $sortedBoardingPasses;
	}

	/**
	 * Describe the steps in the journey as a list.
	 * 
	 * @return array
	 */
	public function describe()
	{
		$list = [];

		$step = 1;

		foreach ($this->passes as $pass) {

			$transport = isset($pass['transport_number']) ? 
				"{$pass['transport_type']} {$pass['transport_number']}" : 
				"the {$pass['transport_type']}";

			$seat = isset($pass['seat_number']) ? 'Sit in ' . $pass['seat_number'] . '.' : 'No seat assignment.';

			$description = "$step. " .
				($pass['transport_type'] == 'flight' ? 
					"From {$pass['from']}, take flight {$pass['transport_number'] } to {$pass['to']}. Gate {$pass['gate']}, Seat {$pass['seat_number']}." : 
					"Take $transport from {$pass['from'] } to {$pass['to']}. $seat") .
				(isset($pass['more_info']) ? ' ' . $pass['more_info'] : '');	

			$list[] = $description;

			$step++;
		}

		$list[] = "$step. You have arrived at your destination.";

		return $list;
	}
}
