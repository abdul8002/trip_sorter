<?php

namespace App\Contracts;

interface JourneyDescriptionInterface
{
	/**
	 * Describe a journey given an ordered list of boarding passes.
	 * 
	 * @return array
	 */
	public function describe();
}