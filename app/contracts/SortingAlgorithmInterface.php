<?php

namespace App\Contracts;

interface SortingAlgorithmInterface
{
	/**
	 * Sort a list of boarding passes in from/to order.
	 * 
	 * @return array
	 */
	public function sort();
}