<?php

namespace App;

use App\Contracts\SortingAlgorithmInterface;

class LinearPathSort implements SortingAlgorithmInterface
{
	/**
	 * The array of boarding passes.
	 * 
	 * @var array
	 */
	protected $boardingPasses;
	
	/**
	 *
	 * @param array $passes 
	 */
	public function __construct($passes)
	{
		$this->boardingPasses = $passes;
	}

	/**
	 * Sort the list of boarding passes.
	 * 
	 * @return array
	 */
	public function sort()
	{
		$destinations = [];
		$journeys = [];
		$start;

		foreach($this->boardingPasses as $pass) {
			$to = $pass['to'];
			$from = $pass['from'];

			$journeys[$from] = $pass;
			$destinations[$to] = true;
		}

		foreach($this->boardingPasses as $pass) {
			$from = $pass['from'];
			$to = $pass['to'];

			echo "$to \n";
			if ( !isset($destinations[$from]) ) {
				$start = $journeys[$from];

				break;
			}
		}

		$sorted[] = $start;
		$next = $start['to'];

		while( isset($journeys[$next]) ) {
			$sorted[] = $journeys[$next];
			$next = $journeys[$next]['to'];
		}

		return $sorted;
	}
}
