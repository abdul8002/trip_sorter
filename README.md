# Trip Sorter

A simple application that will sort an unsorted and unbroken chain of boarding passes into the right order.

# Setup

The project uses composer for dependencies (PHPUnit) and autoloading.

1. [Installing composer is easy](https://getcomposer.org/download/)
2. Install the dependencies `composer install`
3. Run the tests `vendor/bin/phpunit`

# Usage

There are two main classes which can be found in the app directory.

* LinearPathSort.php - will sort an unordered list of boarding passes
* JourneyList.php - will give a description (in list format) for a sorted list of bording passes

Example:

```
$unsortedBoardingPasses = [
	['from' => 'a', 'to' => 'b', 'transport_type' => 'train', 'transport_number' => '78A', 'seat_number' => '45B'],
	['from' => 'c', 'to' => 'd', 'transport_type' => 'airport bus', 'transport_number' => null],
	['from' => 'b', 'to' => 'c', 'transport_type' => 'flight', 'transport_number' => 'SK455', 'seat_number' => '3A', 'gate' => '45B', 'more_info' => 'Baggage drop at ticket counter 344.'],
	['from' => 'd', 'to' => 'e', 'transport_type' => 'flight', 'transport_number' => 'SK22', 'seat_number' => '7B', 'gate' => '22', 'more_info' => 'Baggage will we automatically transferred from your last leg.']
];

$sorted = (new LinearPathSort($unsortedBoardingPasses))->sort();

// Now let's get a description
$description = (new JourneyList($sorted))->describe();

```
# Extendability

Both classes abide by contracts defined in app/contracts. We can easily switch the implementation for the sorting algorithm and the description. 

# Assumptions

1. The complete journey is an unbroken chain
2. There are no loops (i.e. the same destination doesn't appear more than once)
